<?php 
$pais= new pais();
$paises= $pais->consultarTodos();
/*
include 'presentacion/encabezado.php';
$vuelo = new Vuelo();
$vuelos = $vuelo -> consultarTodos(); */
?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consultar Vuelo</h5>
				
				<div class="col-4 text-center">
					<label class="form-label">Pais</label>
					<select class="form-select" name="country">
					<?php 
						foreach ($pais as $paisActual)
						echo "<option value='".$paisActual->getId()."'>".$paisActual->getCod()."--".$paisActual->getNombre()."</option>";
					?>
					</select>
				</div>

				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th scope="col" rowspan="2" class="text-center">#</th>
								<th scope="col" colspan="4" class="text-center table-warning">Itinerario</th>
								<th scope="col" colspan="2" class="text-center table-success">Origen</th>
								<th scope="col" colspan="2" class="text-center table-info">Destino</th>
							</tr>
							<tr>								
								<th scope="col">Fecha Salida</th>
								<th scope="col">Hora Salida</th>
								<th scope="col">Fecha Llegada</th>
								<th scope="col">Hora Llegada</th>
								<th scope="col">Aeropuerto</th>
								<th scope="col">Ciudad</th>
								<th scope="col">Aeropuerto</th>
								<th scope="col">Ciudad</th>
							</tr>
						</thead>
						<tbody>
							<?php /*
							$i = 1;
							foreach ($vuelos as $vueloActual){
							    echo "<tr>";
							    echo "<td>" . $i++ . "</td>";
							    echo "<td>" . $vueloActual -> getFecha_salida() . "</td>";
							    echo "<td>" . $vueloActual -> getHora_salida() . "</td>";
							    echo "<td>" . $vueloActual -> getFecha_llegada() . "</td>";
							    echo "<td>" . $vueloActual -> getHora_llegada() . "</td>";
							    echo "<td>" . $vueloActual -> getRuta() -> getAeropuerto_origen() -> getNombre() . " (" . $vueloActual -> getRuta() -> getAeropuerto_origen() -> getCodigo()  . ")</td>";
							    echo "<td>" . $vueloActual -> getRuta() -> getAeropuerto_origen() -> getCiudad() -> getNombre() . "</td>";
							    echo "<td>" . $vueloActual -> getRuta() -> getAeropuerto_destino() -> getNombre() . " (" . $vueloActual -> getRuta() -> getAeropuerto_destino() -> getCodigo()  . ")</td>";
							    echo "<td>" . $vueloActual -> getRuta() -> getAeropuerto_destino() -> getCiudad() -> getNombre()  . "</td>";
							    echo "<td><a href='index.php?pid=" . base64_encode("presentacion/vuelo/editarVuelo.php") . "&idVuelo=" . $vueloActual -> getId() . "'><i class='fas fa-edit'></i></a></td>";
							    echo "</tr>";							    
							}*/
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>