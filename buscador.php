<?php
$box = $_POST['pais'];
//echo "box =" . $box . "<br>";
$pais= new pais($box, "", "");
$pais->consultar();
$co = $pais->getid_region_region();
$region= new region($co, "");
$region->consultar();
$reporte= new reporte("", "", "", "", "", $box);
$reporte->consultar();
$reportes= $reporte->consultarTodos();

$casusAcumTemp=0;
$casosAcum=0;
$muerteAcumTemp=0;
$muerteAcum=0;
?>

<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consultar Pais</h5>  

				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th scope="col" rowspan="2" class="text-center">#</th>
								<th scope="col" colspan="3" class="text-center table-warning">Pais</th>
								<th scope="col" colspan="2" class="text-center table-success">Acumulados</th>
								<th scope="col" colspan="2" class="text-center table-info">Reciente</th>
							</tr>
							<tr>								
								<th scope="col">Region</th>
								<th scope="col">Codigo</th>
								<th scope="col">Nombre</th>
								<th scope="col">Casos acumulados</th>
								<th scope="col">Muertes acumuladas</th>
								<th scope="col">Casos del ultimo dia reportado</th>
								<th scope="col">Muertes del ultimo dia reportado</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i = 1;
							
							    echo "<tr>";
							    echo "<td>" . $i++ . "</td>";
							    echo "<td>" . $pais->getid_region_region() . "</td>";
							    echo "<td>" . $pais -> getid_country() . "</td>";
								echo $pais -> getid_country();
							    echo "<td>" . $pais -> getname() . "</td>";
								foreach ($reportes as $reporteActual){
									$casusAcumTemp = $reporteActual->getcumulative_cases();
									if($casusAcumTemp > $casosAcum){
										$casosAcum = $casusAcumTemp;
									}
								}
								echo "<td>" . $casosAcum . "</td>";
								foreach ($reportes as $reporteActual){
									$muerteAcumTemp = $reporteActual->getcumulative_deaths();
									if($muerteAcumTemp > $muerteAcum){
										$muerteAcum = $muerteAcumTemp;
									}
								}
								echo "<td>" . $muerteAcum . "</td>";
                            /*
							$i = 1;
							foreach ($vuelos as $vueloActual){
							    echo "<tr>";
							    echo "<td>" . $i++ . "</td>";
							    echo "<td>" . $vueloActual -> getFecha_salida() . "</td>";
							    echo "<td>" . $vueloActual -> getHora_salida() . "</td>";
							    echo "<td>" . $vueloActual -> getFecha_llegada() . "</td>";
							    echo "<td>" . $vueloActual -> getHora_llegada() . "</td>";
							    echo "<td>" . $vueloActual -> getRuta() -> getAeropuerto_origen() -> getNombre() . " (" . $vueloActual -> getRuta() -> getAeropuerto_origen() -> getCodigo()  . ")</td>";
							    echo "<td>" . $vueloActual -> getRuta() -> getAeropuerto_origen() -> getCiudad() -> getNombre() . "</td>";
							    echo "<td>" . $vueloActual -> getRuta() -> getAeropuerto_destino() -> getNombre() . " (" . $vueloActual -> getRuta() -> getAeropuerto_destino() -> getCodigo()  . ")</td>";
							    echo "<td>" . $vueloActual -> getRuta() -> getAeropuerto_destino() -> getCiudad() -> getNombre()  . "</td>";
							    echo "<td><a href='index.php?pid=" . base64_encode("presentacion/vuelo/editarVuelo.php") . "&idVuelo=" . $vueloActual -> getId() . "'><i class='fas fa-edit'></i></a></td>";
							    echo "</tr>";							    
							}*/
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>