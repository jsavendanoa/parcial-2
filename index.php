<?php
include 'presentacion/encabezado.php';
include 'Logica/pais.php';
include 'Logica/region.php';
include 'Logica/reporte.php';
$pais= new pais();
$paises= $pais->consultarTodos();

$pid ="";
if(isset($_GET["pid"])){
  $pid= base64_decode( $_GET["pid"] );
}
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" >
  </head>
  
<?php

    if($pid!=""){
      include $pid;
    } 
?>

<div class="container">
	<div class="row mt-3">

        <form action="index.php?pid= <?php echo base64_encode("buscador.php") ?>"" method="POST">
            <div class="mb-4 text-center">
                <label class="form-label">Pais</label>
                <select class="form-select" name="pais">
                <?php 
                
                    foreach ($paises as $paisActual)
                    echo "<option value= '".$paisActual->getid_country()."' >".$paisActual->getid_country() . " = ".$paisActual->getname()."</option>";
                ?>
                </select>
            </div>

            <div class="col-2 text-center">
                <a href="index.php?pid= <?php echo base64_encode("buscador.php") ?>"><button type="submit" class="btn btn-primary" value="buscar">Buscar</button></a>
            </div>
        </form>
	</div>
</div>


</html>

