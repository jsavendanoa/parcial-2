<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/reporteDAO.php';

class reporte{
    private $id_report;
    private $date;
    private $new_cases;
    private $cumulative_cases;
    private $new_deaths;
    private $cumulative_deaths;
    private $id_country_country;
    
    private  $conexion;
    private  $paisDAO;
    

    public function getid_report()
    {
        return $this->id_report;
    }

    public function getdate()
    {
        return $this->date;
    }

    public function getnew_cases()
    {
        return $this->new_cases;
    }

    public function getcumulative_cases()
    {
        return $this->cumulative_cases;
    }

    public function getnew_deaths()
    {
        return $this->new_deaths;
    }

    public function getcumulative_deaths()
    {
        return $this->cumulative_deaths;
    }
    
    public function getid_country_country()
    {
        return $this->id_country_country;
    }

    public function __construct($id_report="", $date="", $new_cases="", $cumulative_cases="", $new_deaths="", $cumulative_deaths="", $id_country_country=""){
        $this->id_report=$id_report;
        $this->date=$date;
        $this->new_cases=$new_cases;
        $this->cumulative_cases=$cumulative_cases;
        $this->new_deaths=$new_deaths;
        $this->cumulative_deaths=$cumulative_deaths;
        $this->id_country_country=$id_country_country;
        $this->conexion= new Conexion();
        $this->paisDAO=new reporteDAO($this->id_report, $this->date, $this->new_cases, $this->cumulative_cases, $this->new_deaths, $this->cumulative_deaths, $this->id_country_country);
        
    }

    
    public function consultar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->paisDAO->consultar());
        $registro= $this->conexion->extraer();
        $this->id_report=$registro[0];
        $this->date=$registro[1];
        $this->new_cases=$registro[2];
        $this->cumulative_cases=$registro[3];
        $this->new_deaths=$registro[4];
        $this->cumulative_deaths=$registro[5];
        $this->conexion->cerrar();
       
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> paisDAO -> consultarTodos()); 
        $reportes = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $this->id_report=$registro[0];
            $this->date=$registro[1];
            $this->new_cases=$registro[2];
            $this->cumulative_cases=$registro[3];
            $this->new_deaths=$registro[4];
            $this->cumulative_deaths=$registro[5];
            $this->id_country_country=$registro[5];
            $reporte = new reporte($registro[0], $registro[1], $registro[2], $registro[3], $registro[4], $registro[5], $registro[6]); 
            array_push($reportes, $reporte);
        }
        $this -> conexion -> cerrar();
        return  $reportes;
    }
   
}
?>
