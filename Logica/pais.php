<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/paisDAO.php';

class pais{
    private $id_country;
    private $name;
    private $id_region_region;
    
    private  $conexion;
    private  $paisDAO;
    

    public function getid_country()
    {
        return $this->id_country;
    }

    public function getname()
    {
        return $this->name;
    }


    public function getid_region_region()
    {
        return $this->id_region_region;
    }
   

    public function __construct($id_country="", $name="",$id_region_region=""){
        $this->id_country=$id_country;
        $this->name=$name;
        $this->id_region_region=$id_region_region;
        $this->conexion= new Conexion();
        $this->paisDAO=new paisDAO($this->id_country, $this->name, $this->id_region_region);
        
    }

    
    public function consultar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->paisDAO->consultar());
        $registro= $this->conexion->extraer();
        $this->name=$registro[0];
        $this->id_region_region=$registro[1];
        $this->conexion->cerrar();
       
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> paisDAO -> consultarTodos()); 
        $paises = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $this->id_country=$registro[0];
            $this->name=$registro[1];
            $this->id_region_region=$registro[2];
            $pais = new pais($registro[0], $registro[1], $registro[2]); 
            array_push($paises, $pais);
        }
        $this -> conexion -> cerrar();
        return  $paises;
    }
   
}
?>
