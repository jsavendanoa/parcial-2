<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/regionDAO.php';

class region{
    private $id_region;
    private $name;
    
    private  $conexion;
    private  $regionDAO;
    

    public function getid_region()
    {
        return $this->id_region_region;
    }

    public function getname()
    {
        return $this->name;
    }


    
   

    public function __construct($id_region="", $name=""){
        $this->id_region_region=$id_region;
        $this->name=$name;
        
        $this->conexion= new Conexion();
        $this->regionDAO=new regionDAO($this->id_region, $this->name);
        
    }

    
    public function consultar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->regionDAO->consultar());
        $registro= $this->conexion->extraer();
        $this->name=$registro;
        $this->conexion->cerrar();
       
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> regionDAO -> consultarTodos()); 
        $regiones = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $this->id_region=$registro[0];
            $this->name=$registro[1];
            $region = new region($registro[0], $registro[1]); 
            array_push($regiones, $region);
        }
        $this -> conexion -> cerrar();
        return  $regiones;
    }
   
}
?>
